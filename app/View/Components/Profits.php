<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Cache;
use App\Account;
use Carbon\Carbon;

class Profits extends Component
{
  public $rawProfitsOfYear;
  public $profitsOfYear;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
      [$businessDigit, $profits] = Cache::remember('profits_of_year_' . auth()->user()->id, 3600, function () {
        $businessDigit = Account::where('user_id', '=', auth()->user()->id)->sum('business_digit');
        $profits = Account::where('user_id', '=', auth()->user()->id)->sum('profits');
        return [$businessDigit, $profits];
      });

      $this->rawProfitsOfYear = number_format_fr($businessDigit);
      $this->profitsOfYear = number_format_fr($profits);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.profits');
    }
}
