<?php

if (!function_exists('mb_trans')) {
  function mb_trans($textName, $pluralize = null, $params = [], $ucfirst = true)
  {
    if ($pluralize == null && empty($params) && !$ucfirst) {
      return __($textName);
    }

    $string = __($textName);

    if ($pluralize == null && !empty($params)) {
      $string = __($textName, $params);
    } elseif ($pluralize != null && empty($params)) {
      $string = trans_choice($textName, $pluralize);
    } elseif ($pluralize != null && !empty($params)) {
      $string = trans_choice($textName, $pluralize, $params);
    }

    if (!$ucfirst) {
      return $string;
    }
    return mb_ucfirst($string);
  }
}

if (!function_exists('intToBool')) {
  function intToBool($value)
  {
    return ($value?true:false);
  }
}
if (!function_exists('getDiscountType')) {
  function getDiscountType()
  {
    return [
      '%' => '%',
      '€' => '€'
    ];
  }
}
if (!function_exists('getDiscountMode')) {
  function getDiscountMode()
  {
    return [
      '<' => '<',
      '>' => '>',
      '=' => '='
    ];
  }
}
if (!function_exists('defineReference')) {
  function defineReference($nb)
  {
    $nb = $nb+1;
    if ($nb < 10) {
      return '00000' . $nb;
    }
    if ($nb >= 10 && $nb < 100) {
      return '0000' . $nb;
    }
    if ($nb >= 100 && $nb < 1000) {
      return '000' . $nb;
    }
    if ($nb >= 1000 && $nb < 10000) {
      return '00' . $nb;
    }
    if ($nb >= 10000 && $nb < 100000) {
      return '0' . $nb;
    }
    if ($nb >= 100000 && $nb < 1000000) {
      return $nb;
    }
  }
}
if (!function_exists('mb_ucfirst') && function_exists('mb_substr')) {
  /**
   * Uppercase the first character (also special char)
   * @param  [String] $string a string
   * @return [String]         string formatted
   */
    function mb_ucfirst($string) {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
        return $string;
    }
}


if (!function_exists('format_phone_fr')) {
  function format_phone_fr($phone = '')
  {
    return preg_replace("/([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "$1 $2 $3 $4 $5", $phone);
  }
}

if (!function_exists('yesNo')) {
  function yesNo($value)
  {
    if ($value) {
      return mb_trans('text.yes');
    }
    return mb_trans('text.no');
  }
}

if (!function_exists('ends_with')) {
  /**
   * Determines if the given string end with the given value
   *
   * @param  String       $haystack the string to test
   * @param  String|Array $needle   the value at the end
   * @return Boolean                the result
   */
  function ends_with($haystack, $needle = null)
  {
    if (is_array($needle)) {
      foreach ($needle as $value) {
        $length = strlen($value);
        return (substr($haystack, -$length) === $value);
      }
    }

    $length = strlen($needle);

    if ($length == 0) {
        throw new \Exception("Error : the second parameter in ends_with must not be null", 1);
    }

    return (substr($haystack, -$length) === $needle);
  }
}

if (!function_exists('setCreateLink')) {
  function setCreateLink()
  {
    $path = Request::path();
    if (ends_with($path, '/edit')) {
      $segment = explode('/', $path);

      if (in_array($segment[0], config('app.alt_langs')) ) {
        $route = $segment[1];
      } else {
        $route = $segment[0];
      }

      return '/' . $route .'/create';
    }

    if ($path === '/') {
      return '/invoice/create';
    }

    return '/' . $path .'/create';
  }
}

if (!function_exists('number_format_fr')) {
  function number_format_fr($number)
  {
    return number_format($number, 2, ',', ' ');
  }
}
