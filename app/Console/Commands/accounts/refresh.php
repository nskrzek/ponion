<?php

namespace App\Console\Commands\accounts;

use Illuminate\Support\Facades\Cache;
use Illuminate\Console\Command;
use App\{ Invoice, Account };
use Carbon\Carbon;

class refresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:refresh {id? : The ID of the account}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh business digit and profits of account(s)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $accountId = $this->argument('id');

      if ($accountId) {
        $account = Account::find($accountId);

        $this->refresh($account);
      } else {
        $accounts = Account::get();

        foreach ($accounts as $account) {
          $this->refresh($account);
        }
      }
    }

    protected function refresh($account)
    {
      $businessDigit = Invoice::where(function ($q) use ($account) {
        $q->where('type', '=', 'invoice');
        $q->where('isPaid', '=', 1);
        $q->where('account_id', '=', $account->id);
        $q->where('updated_at', 'LIKE' , Carbon::now()->year . '-%');
      })->sum('total');

      $businessDigitAfterTaxing = ($account->taxe/100) * $businessDigit;
      $profitsOfYear = $businessDigit - $businessDigitAfterTaxing;

      $account->business_digit = $businessDigit;
      $account->profits = round($profitsOfYear, 2);
      $account->save();

      $this->info("Mise à jour du compte {$account->name}");
      $this->info("Chiffre d'affaire : {$businessDigit}");
      $this->info("Bénéfice : " . round($profitsOfYear, 2));
      $this->newLine();
    }
}
