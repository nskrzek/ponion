<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
  public function invoice()
  {
    return $this->belongsToMany(Invoice::class);
  }
}
