<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  public function invoice()
  {
    return $this->hasMany(Invoice::class);
  }
  public function user()
  {
    return $this->belongsToMany(User::class);
  }
}
