<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class InvoiceLine extends Model
{
  /**
   * you can override uuid prefix by using $uuidPrefix
   */
  use Uuids;

  protected $uuidPrefix = 'line';

  // Carbon instance fields
  protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date'];

  public function invoice()
  {
    return $this->belongsTo(Invoice::class);
  }

/**
 * Filters the lines
 * @param  object $query  [description]
 * @param  array $filters [description]
 * @return object         [description]
 */
  public function scopeFilter($query, $filters)
  {
    if (array_key_exists('invoice', $filters)) {
      $query->where('invoice_id', $filters['invoice']);
    }
  }
}
