<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
  /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
  protected $fillable = [
      'name',
      'business_digit',
      'profits',
      'spending',
      'taxe',
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
