<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Invoice extends Model
{
  /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
  protected $fillable = [
      'type',
      'customer_id',
      'service_id',
      'number',
      'date_devis',
      'date_invoice',
      'customer',
      'service',
      'isAccepted',
      'isPaid',
      'note',
      'unit_type',
      'account_id',
  ];
  /**
   * you can override uuid prefix by using $uuidPrefix
   */
  use Uuids;

  protected $uuidPrefix = 'invoice';

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName()
  {
      return 'uuid';
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
  public function customer()
  {
    return $this->belongsTo(Customer::class);
  }
  public function service()
  {
    return $this->belongsTo(Service::class);
  }
  public function line()
  {
    return $this->hasMany(InvoiceLine::class);
  }

  /**
   * Filters the lines
   * @param  object $query  [description]
   * @param  array $filters [description]
   * @return object         [description]
   */
    public function scopeFilter($query, $filters)
    {
      if (array_key_exists('type', $filters)) {
        if ($filters['type'] != '') {
          $query->where('type', $filters['type']);
        }
      }
      if (array_key_exists('isAccepted', $filters)) {
        if ($filters['isAccepted'] != '') {
          $query->where('isAccepted', $filters['isAccepted']);
        }
      }
      if (array_key_exists('isPaid', $filters)) {
        if ($filters['isPaid'] != '') {
          $query->where('isPaid', $filters['isPaid']);
        }
      }
    }
}
