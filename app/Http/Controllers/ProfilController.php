<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ProfilController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('profil.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('profil.index')->with([
          'user' => User::find($id)
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $profil = User::find($id);
        return view('profil.edit')->with([
          'profil' => $profil
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $profil = User::find($id);

      $profil->name = $request->name;
      $profil->email = $request->email;
      $profil->address = $request->address;
      $profil->cp = $request->cp;
      $profil->city = $request->city;
      $profil->contact = $request->contact;
      $profil->phone = $request->phone;
      $profil->website = $request->website;
      $profil->id_society = $request->id_society;

      $profil->save();

      return view('profil.index')->with([
          'user' => User::find($id)
      ]);
    }
}
