<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InvoiceLine;
use App\Models\Invoice;

class InvoiceLineController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $params = $request->only(['invoice']);
    return InvoiceLine::filter($params)->get();
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $invoiceLine = new InvoiceLine;

      $invoiceLine->invoice_id = $request->invoiceId;
      $invoiceLine->date = $request->date??Carbon::now();
      // $invoiceLine->isCharged = intToBool($request->isCharged);
      $invoiceLine->description = $request->description;
      $invoiceLine->quatity = $request->quatity;
      $invoiceLine->unit_price = $request->unitPrice;
      $invoiceLine->discount_type = $request->discountType;
      $invoiceLine->discount_mode = $request->discountMode;
      $invoiceLine->discount = $request->discount;
      // $invoiceLine->isTaxable = intToBool($request->isTaxable);
      // $invoiceLine->taxes_included = intToBool($request->taxesIncluded);
      // $invoiceLine->taxe = $request->taxe;
      $invoiceLine->sub_total = $request->subTotal;

      $invoiceLine->save();

      $invoice = Invoice::find($request->invoiceId);
      $invoice->total_brut = $invoice->line->sum('sub_total');
      $invoice->total = $invoice->total_brut * (1 + ($invoice->taxe/100));
      $invoice->save();

      return response()->json([
          'message' => 'ok',
        ], 200);
      // return redirect()->route('invoice', ['id' => $request->$invoiceId]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $invoiceLine = InvoiceLine::find($id);

      $invoiceLine->date = $request->date;
      // $invoiceLine->isCharged = intToBool($request->isCharged);
      $invoiceLine->description = $request->description;
      $invoiceLine->quatity = $request->quatity;
      $invoiceLine->unit_price = $request->unitPrice;
      $invoiceLine->discount_type = $request->discountType;
      $invoiceLine->discount_mode = $request->discountMode;
      $invoiceLine->discount = $request->discount;
      // $invoiceLine->isTaxable = intToBool($request->isTaxable);
      // $invoiceLine->taxes_included = intToBool($request->taxesIncluded);
      // $invoiceLine->taxe = $request->taxe;
      $invoiceLine->sub_total = $request->subTotal;

      $invoiceLine->save();

      $invoice = Invoice::find($invoiceLine->invoice_id);
      $invoice->total_brut = $invoice->line->sum('sub_total');
      $invoice->total = $invoice->total_brut * (1 + ($invoice->taxe/100));
      $invoice->save();

      return response()->json([
          'message' => 'ok',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $invoiceLine = InvoiceLine::find($id);
      $invoiceLine->delete();

      $invoice = Invoice::find(request()->invoice);
      $invoice->total_brut = $invoice->line->sum('sub_total');
      $invoice->total = $invoice->total_brut * (1 + ($invoice->taxe/100));
      $invoice->save();

      return response()->json([
          'message' => 'ok',
        ], 200);
    }
}
