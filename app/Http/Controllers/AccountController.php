<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\View\View;
use Carbon\Carbon;

class AccountController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
      $accounts = Account::get();
      return view('account.index')->with([
          'accounts' => $accounts
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      auth()->user()->newAccount(
        new Account([
          'name' => $request->name,
          'business_digit' => $request->business_digit??null,
          'profits' => $request->profits??null,
          // 'spending' => $request->spending??null,
          'iban' => $request->iban??null,
          'bic' => $request->bic??null,
          'taxe' => $request->taxe??0.0,
        ])
      );

      return redirect('activity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id): View
    {
      return view('account.show')->with([
          'account' => Account::find($id)
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $id)
    {
       return view('account.edit')->with([
           'account' => Account::find($id)
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
      $account = Account::find($id);

      $account->name = $request->name;

      $account->business_digit = $request->business_digit;

      $account->iban = $request->iban;

      $account->bic = $request->bic;

      $account->taxe = $request->taxe;

      $account->profits = $request->profits;

      // $account->spending = $request->spending;

      $account->save();

      return redirect('activity/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $account = Account::find($id);
        $account->delete();

        return redirect('activity');
    }
}
