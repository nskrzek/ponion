<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\{ Customer, Invoice, Service, Account };
use PDF;

class InvoiceController extends Controller
{

  protected $types = [
    '' => 'text.all',
    'invoice' => 'Facture',
    'quote' => 'Devis',
  ];
  protected $unitTypes = [
    'h' => 'Heure',
    'd' => 'Jour',
    'm' => 'Mois',
    'a' => 'Année',
  ];

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $filter = $request->only(['type', 'isAccepted', 'isPaid']);

      $invoices = Invoice::with('service')
        ->where('user_id', '=', auth()->user()->id)
        ->filter($filter)
        ->orderBy('created_at', 'desc')
        ->get();

      return view('invoice.index')->with([
          'invoices' => $invoices,
          'types' => $this->types,
          'filters' => [
            'type' => $filter['type']??'',
            'isAccepted' => $filter['isAccepted']??'',
            'isPaid' => $filter['isPaid']??'',
          ],
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $customers = Customer::with(['invoice'])->get();
      $services = Service::get();
      $invoices = Invoice::get();

      $currentDate = Carbon::now()->format('d/m/Y');

      $number = defineReference($invoices->count());

      $accounts = Account::where('user_id', '=', auth()->user()->id)->get();

        return view('invoice.create')->with([
            'customers' => $customers,
            'currentDate' => $currentDate,
            'number' => $number,
            'services' => $services,
            'types' => $this->types,
            'unitTypes' => $this->unitTypes,
            'defaultType' => 'quote',
            'accounts' => $accounts,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $customers = Customer::find($request->customer);

      $currentDate = Carbon::now()->format('d/m/Y');
      $numberInvoices = Invoice::where('number', 'LIKE', $customers->number.'%')->count();
      $invoiceNumber = $this->formatInvoiceNumber($numberInvoices, $customers->number);

      $invoice = [
        'type' => $request->type,
        'customer_id' => $request->customer,
        'service_id' => $request->service,
        'number' => $invoiceNumber,
        'isAccepted' => (isset($request['isAccepted']) ? $request['isAccepted'] : 0),
        'isPaid' => (isset($request['isAccepted']) ? $request['isAccepted'] : 0),
        'note' => $request->note,
        'unit_type' => $request->unit_type,
        'account_id' => $request->account,
      ];

      if ($request->type === 'quote') {
          $invoice['date_devis'] = $currentDate;
      } elseif ($request->type === 'invoice') {
          $invoice['date_invoice'] = $currentDate;
      }

      auth()->user()->makeInvoice(
        new Invoice($invoice)
      );

      return redirect('invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $invoice = Invoice::find($id);

      $customers = Customer::get();

      $services = Service::get();

      return view('invoice.show')->with([
          'invoice' => $invoice,
          'customers' => $customers,
          'services' => $services,
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $invoice = Invoice::with(['customer', 'service'])->find($id);

      $customers = Customer::get();

      $services = Service::get();

      $accounts = Account::where('user_id', '=', auth()->user()->id)->get();

      return view('invoice.edit', [
          'invoice' => $invoice,
          'customers' => $customers,
          'services' => $services,
          'types' => $this->types,
          'unitTypes' => $this->unitTypes,
          'accounts' => $accounts,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $invoice = Invoice::find($id);

    if ($request->type != $invoice->type ) {

        $currentDate = Carbon::now()->format('d/m/Y');

        if ($request->type === 'quote') {
            $invoice->date_devis = $currentDate;
        } elseif ($request->type === 'invoice') {
            $invoice->date_invoice = $currentDate;
        }

        $invoice->type = $request->type;
    }

      $invoice->service_id = $request->service;
      $invoice->account_id = $request->account;

      if ($request['isAccepted']) {
        $invoice->isAccepted = $request['isAccepted'];
      }

      if ($request['isPaid']) {
        $invoice->isPaid = $request['isPaid'];
        Artisan::call('accounts:refresh', [ 'id' => $invoice->account_id ]);
      }

      $invoice->note = $request->note;

      $invoice->unit_type = $request->unit_type;

      $invoice->save();

      return redirect('invoice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $invoice = Invoice::find($id);
      $invoice->delete();

      return redirect('invoice');
    }

    /**
     * Display invoice details.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($id, Request $request)
    {

      $invoice = Invoice::with(['customer', 'user.account', 'line'])->find($id);

      $type = $this->types[$invoice->type];

      $unitTypes = "d'heures";

      if (isset($this->unitTypes[$invoice->unit_type])) {
          switch ($invoice->unit_type) {
              case 'h':
                   $unitTypes = "d'heures";
                  break;
              case 'd':
                   $unitTypes = "de jours";
                  break;
              case 'm':
                   $unitTypes = "de mois";
                  break;
              case 'a':
                   $unitTypes = "d'années";
                  break;
          }
      }

      $pdf = PDF::fromView('invoice.pdf.' . $invoice->type, [
          'invoice' => $invoice,
          'type' => $type,
          'unitTypes' => strtolower($unitTypes)
      ]);

      return $pdf->stream(mb_strtolower($type) . '_' . $invoice->number . '.pdf');
    }

    protected function formatInvoiceNumber(int $nbInvoice, string $customerNum) : string
    {
      $nbInvoice++;

      if ($nbInvoice < 10) {
        $nbInvoice = '00000' . $nbInvoice;
      } elseif ($nbInvoice >= 10 && $nbInvoice < 100) {
        $nbInvoice = '0000' . $nbInvoice;
      } elseif ($nbInvoice >= 100 && $nbInvoice < 1000) {
        $nbInvoice = '000' . $nbInvoice;
      } elseif ($nbInvoice >= 1000 && $nbInvoice < 10000) {
        $nbInvoice = '00' . $nbInvoice;
      } elseif ($nbInvoice >= 10000 && $nbInvoice < 100000) {
        $nbInvoice = '0' . $nbInvoice;
      } else {
        $nbInvoice = $nbInvoice;
      }

      return $customerNum . $nbInvoice;
    }
}
