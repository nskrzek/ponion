<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $customers = Customer::get();
      return view('customer.index')->with([
          'customers' => $customers
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer;

        $customer->number = $request->number;
        $customer->society_name = $request->society_name??null;
        $customer->contact_name = $request->contact_name??null;
        $customer->address = $request->address??null;
        $customer->city = $request->city??null;
        $customer->postalcode = $request->postalcode??null;
        $customer->phone = $request->phone??null;
        $customer->email = $request->email??null;
        $customer->website = $request->website??null;
        $customer->note = $request->note??null;
        $customer->isActivated = $request->isActivated??true;
        $customer->save();

        return redirect('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('customer.show')->with([
          'customer' => Customer::find($id)
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('customer.edit')->with([
          'customer' => Customer::find($id)
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        $customer->number = $request->number;
        $customer->society_name = $request->society_name??null;
        $customer->contact_name = $request->contact_name??null;
        $customer->address = $request->address??null;
        $customer->city = $request->city??null;
        $customer->postalcode = $request->postalcode??null;
        $customer->phone = $request->phone??null;
        $customer->email = $request->email??null;
        $customer->website = $request->website??null;
        $customer->note = $request->note??null;
        $customer->isActivated = $request->isActivated??true;
        $customer->save();

        return redirect()->route('customer.show', [$customer]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        return redirect()->view('customer');
    }
}
