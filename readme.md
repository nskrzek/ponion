## Prérequits
SQLite3


Activer les extensions php pdo_sqlite et sqlite3
## Installation
1. cp .env.production .env
2. composer install
3. php artisan key:generate
4. php artisan migrate
5. php artisan serve

## Trouble
1. could not find driver (SQL: PRAGMA foreign_keys = ON;)

php-sqlite est mal installer ou l'extension n'est pas active (penser à redémarer la machine après l'avoir fait)

2. Invalid characters passed for attempted conversion, these have been ignored

voir [ici](https://github.com/dompdf/dompdf/issues/2003)
