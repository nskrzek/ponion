@extends('layouts.app')
@section('content')
  <form class="" action="/profil/{{ $profil->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group mb-3">
        <label for="name" class="form-label">{{ mb_trans('text.name') }}</label>
      <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $profil->name) }}" placeholder="{{ mb_trans('text.name') }}">
    </div>
    <div class="form-group row mb-3">
        <div class="col-md-4">
            <label for="phone" class="form-label">{{ mb_trans('text.phone') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone', $profil->phone) }}" placeholder="{{ mb_trans('text.phone') }}">
            </div>
        </div>
      <div class="col-md-4">
          <label for="email" class="form-label">{{ mb_trans('text.email') }}</label>
          <div class="input-group">
              <input type="text" class="form-control" name="email" id="email" value="{{ old('email', $profil->email) }}" placeholder="{{ mb_trans('text.email') }}">
          </div>
      </div>
      <div class="col-md-4">
          <label for="website" class="form-label">{{ mb_trans('text.website') }}</label>
          <div class="input-group">
              <input type="text" class="form-control" name="website" id="website" value="{{ old('website', $profil->website) }}" placeholder="{{ mb_trans('text.website') }}">
          </div>
      </div>
    </div>
    <div class="form-group mb-3">
        <label for="address" class="form-label">{{ mb_trans('text.address') }}</label>
      <input type="text" class="form-control" name="address"  id="address" value="{{ old('address', $profil->address) }}" placeholder="{{ mb_trans('text.address') }}">
    </div>
    <div class="form-group row mb-3">
        <div class="col-md-6">
            <label for="cp" class="form-label">{{ mb_trans('text.postalcode') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="cp" id="cp" value="{{ old('cp', $profil->cp) }}" placeholder="{{ mb_trans('text.postalcode') }}">
            </div>
        </div>
        <div class="col-md-6">
            <label for="city" class="form-label">{{ mb_trans('text.city') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="city" id="city" value="{{ old('city', $profil->city) }}" placeholder="{{ mb_trans('text.city') }}">
            </div>
        </div>
    </div>
    <div class="form-group mb-3">
        <label for="id_society" class="form-label">{{ mb_trans('text.id_society') }}</label>
      <input type="text" class="form-control" name="id_society" id="id_society" value="{{ old('id_society', $profil->id_society) }}" placeholder="{{ mb_trans('text.id_society') }}">
    </div>
    <a href="/profil/{{ $profil->id }}" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
    <button type="submit" class="btn btn-primary">{{ mb_trans('text.update') }}</button>
  </form>
@endsection
