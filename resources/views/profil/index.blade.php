@extends('layouts.app')
@section('content')
  <div id="profile">
    <div class="card">
      <div class="card-header">
        <h3>{{ $user->name }}</h3>
      </div>
      <div class="card-body">
        <ul>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.email') }} :</span><span class="col-sm-6">{{ $user->email??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.address') }} :</span><span class="col-sm-6">{{ $user->address??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.postalcode') }} :</span><span class="col-sm-6">{{ $user->cp??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.city') }} :</span><span class="col-sm-6">{{ $user->city??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.phone') }} :</span><span class="col-sm-6">{{ $user->phone ? format_phone_fr($user->phone)  : '-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.website') }} :</span><span class="col-sm-6">
            @if ($user->website)
              <a href="//{{ $user->website }}" target="_blank">{{ $user->website }}</a>
            @else
              -
            @endif
            </span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.id_society') }} :</span><span class="col-sm-6">{{ $user->id_society??'-' }}</span></li>
        </ul>
      </div>
      <div class="card-footer">
        <a class="btn btn-primary" href="/profil/{{ $user->id }}/edit">{{ mb_trans('text.edit') }}</a>
      </div>
    </div>
  </div>
@endsection
