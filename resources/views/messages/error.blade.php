@if ($errors->any())
<div class="alert alert-danger" role="alert">
    @isset($title)
    <h4 class="alert-heading">{{ $title }}</h4>
    @endisset
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ mb_ucfirst($error) }}</li>
        @endforeach
    </ul>
</div>
@endif
