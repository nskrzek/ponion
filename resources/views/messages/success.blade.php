@if (session()->has('success'))
<div class="alert alert-success" role="alert">
    @isset ($title)
    <h4 class="alert-heading">{{ $title }}</h4>
    @endisset
    <p>{{ session()->get('success') }}</p>
</div>
@endif
