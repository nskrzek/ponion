<header class="infos-bar">
  <div class="infos">
  @if (Auth::check())
      <div class="menu">
        @include('layouts.menu')
      </div>
      <x-profits></x-profits>
      <dropdown-profile user-name="{{ Auth::user()->name }}" user-id="{{ auth()->user()->id}}"></dropdown-profile>
  @else
    <div class="menu">
    @if (Request::is('login'))
      <a href="/register">{{ mb_trans('text.register') }}</a>
    @else
      <a href="/login">{{ mb_trans('text.login') }}</a>
    @endif
    </div>
  @endif
</div>
@if (Auth::check())
    <form name="logout" action="/logout" method="post">
        @csrf
    </form>
@endif
</header>
