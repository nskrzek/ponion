<a class="item" href="/invoice">
  <span>
    {{ mb_trans('text.billing', 2) }}
  </span>
  {{-- <i class="far fa-list-alt"></i> --}}
</a>
<a class="item" href="/activity">
  <span>
    {{ mb_trans('text.activities', 2) }}
  </span>
  {{-- <i class="far fa-chart-bar"></i> --}}
</a>
<a class="item" href="/customer">
  <span>
    {{ mb_trans('text.customer', 2) }}
  </span>
  {{-- <i class="far fa-address-book"></i> --}}
</a>
