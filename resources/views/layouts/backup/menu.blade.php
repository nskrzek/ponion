<b-dropdown size="sm" id="dropdownMenuButton" text="Menu">
  @foreach ($menu as $element)
    <b-dropdown-item class="{{ (Request::is([$element['url'], $element['url'] . '/*'])) ? ' active' : '' }}" href="/{{$element['url']}}">
      @if (in_array($element['name'], ['customer', 'invoice']))
        {{ mb_trans('text.' . $element['name'], 2) }}
      @else
        {{ mb_trans('text.' . $element['name'], 1) }}
      @endif
    </b-dropdown-item>
  @endforeach
  <b-dropdown-divider></b-dropdown-divider>
  <b-dropdown-item class="{{ (Request::is(['profil', 'profil/*'])) ? ' active' : '' }}" href="/profil/{{ auth()->user()->id}}">{{ mb_trans('text.profil') }}</b-dropdown-item>
  <b-dropdown-form class="m-0 p-0" action="/logout" method="post">
    @csrf
    <b-dropdown-item-button type="submit">{{ mb_trans('text.logout') }}</b-dropdown-item-button>
  </b-dropdown-form>
</b-dropdown>
