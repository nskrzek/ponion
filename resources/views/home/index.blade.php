@extends('layouts.app')
@section('content')
  <div id="home" class="row">
    @foreach ($menu as $element)
      @if ($element['name'] != 'home')
        <div class="card ed-card col-md-5">
          <div class="card-header">
            <i class="{{$element['icon']}}"></i>
          </div>
          <div class="content">
            @if (in_array($element['name'], ['customer', 'invoice']))
              {{ mb_trans('text.' . $element['name'], 2) }}
            @else
              {{ mb_trans('text.' . $element['name'], 1) }}
            @endif
          </div>
          <div class="card-footer">
            <a href="{{$element['url']}}">Accéder</a>
          </div>
        </div>
      @endif
    @endforeach
    <div class="card ed-card col-md-5">
      <div class="card-header">
        <i class="far fa-id-card"></i>
      </div>
      <div class="content">
        {{ mb_trans('text.profil') }}
      </div>
      <div class="card-footer">
        <a href="/profil/{{ auth()->user()->id}}">Accéder</a>
      </div>
    </div>
  </div>
@endsection
