@extends('layouts.app')
@section('content')
  <div id="profile">
    <div class="card">
      <div class="card-header">
        <h3>#{{ $customer->number }}</h3>
      </div>
      <div class="card-body">
        <h5>{{ mb_trans('text.society') }}</h5>
        <ul>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.name_society') }} :</span><span class="col-sm-6">{{ $customer->society_name??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.address') }} :</span><span class="col-sm-6">{{ $customer->address??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.city') }} :</span><span class="col-sm-6">{{ $customer->city??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.postalcode', 2) }} :</span><span class="col-sm-6">{{ $customer->postalcode??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.website', 2) }} :</span><span class="col-sm-6">{{ $customer->website??'-' }}</span></li>
        </ul>
          <h5>{{ mb_trans('text.contact_society') }}</h5>
          <ul>
            <li class="row"><span class="col-sm-6">{{ mb_trans('text.name') }}</span><span class="col-sm-6">{{ $customer->contact_name??'-' }}</span></li>
            <li class="row"><span class="col-sm-6">{{ mb_trans('text.phone') }}</span><span class="col-sm-6">{{ $customer->phone??'-' }}</span></li>
            <li class="row"><span class="col-sm-6">{{ mb_trans('text.email') }}</span><span class="col-sm-6">{{ $customer->email??'-' }}</span></li>
          </ul>
            <h4>{{ mb_trans('text.complement') }}</h4>
            <ul>
              <li class="row"><span class="col-sm-6">{{ mb_trans('text.note') }}</span><span class="col-sm-6">{{ $customer->note??'-' }}</span></li>
              <li class="row"><span class="col-sm-6">{{ mb_trans('text.activated') }}</span><span class="col-sm-6">{{ yesNo($customer->isActivated) }}</span></li>
            </ul>
      </div>
      <div class="card-footer">
        <a href="/customer" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
        <a class="btn btn-primary" href="/customer/{{ $customer->id }}/edit">{{ mb_trans('text.edit') }}</a>
      </div>
    </div>
  </div>
@endsection
