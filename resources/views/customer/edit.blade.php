@extends('layouts.app')
@section('content')
  <form class="" action="/customer/{{$customer->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group mb-3">
      <div class="input-group">
        <div class="input-group-prepend"><span class="input-group-text">#</span></div>
        <input class="form-control" type="text" name="number" value="{{ old('number', $customer->number) }}" placeholder="{{ mb_trans('text.customer_number') }}">
      </div>
    </div>
    <div class="form-group mb-3">
      <h4>{{ mb_trans('text.society') }}</h4>
      <input class="form-control my-1" type="text" name="society_name" value="{{ old('society_name', $customer->society_name) }}" placeholder="{{ mb_trans('text.name_society') }}">
      <input class="form-control my-1" type="text" name="address" value="{{ old('address', $customer->address) }}" placeholder="{{ mb_trans('text.address') }}">
      <div class="input-group my-1">
        <input class="form-control" type="text" name="city" value="{{ old('city', $customer->city) }}" placeholder="{{ mb_trans('text.city') }}">
        <input class="form-control" type="number" name="postalcode" value="{{ old('postalcode', $customer->postalcode) }}" placeholder="{{ mb_trans('text.postalcode') }}">
      </div>
      <div class="input-group my-1">
        <div class="input-group-prepend"><span class="input-group-text">URL</span></div>
        <input class="form-control" type="url" name="website" value="{{ old('website', $customer->website) }}" placeholder="{{ mb_trans('text.website') }}">
      </div>
    </div>
    <div class="form-group mb-3">
      <h4>{{ mb_trans('text.contact_society') }}</h4>
      <input class="form-control my-1" type="text" name="contact_name" value="{{ old('contact_name', $customer->contact_name) }}" placeholder="{{ mb_trans('text.name') }}">
      <input class="form-control my-1" type="tel" name="phone" value="{{ old('phone', $customer->phone) }}" placeholder="{{ mb_trans('text.phone') }}">
      <div class="input-group my-1">
        <div class="input-group-prepend"><span class="input-group-text">@</span></div>
        <input class="form-control" type="email" name="email" value="{{ old('email', $customer->email) }}" placeholder="{{ mb_trans('text.email') }}">
      </div>
    </div>
    <div class="form-group mb-3">
      <h4>{{ mb_trans('text.complement') }}</h4>
      <textarea class="form-control" name="note" rows="8" cols="80" placeholder="{{ mb_trans('text.note') }}">{{ old('note', $customer->note) }}</textarea>
    </div>
    <div class="form-group mb-3">
      <div class="form-check">
        <x-form.checkbox name="isActivated" :label="mb_trans('text.activated')" :checked="old('isActivated', $customer->isActivated)" />
      </div>
    </div>
    <a href="/customer/{{$customer->id}}" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
    <button type="submit" class="btn btn-primary">{{ mb_trans('text.update') }}</button>
  </form>
@endsection
