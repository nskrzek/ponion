@extends('layouts.app')
@section('content')
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>{{ mb_trans('text.name_society') }}</th>
        <th>{{ mb_trans('text.contact_society') }}</th>
        <th>{{ mb_trans('text.address') }}</th>
        <th>{{ mb_trans('text.city') }}</th>
        <th>{{ mb_trans('text.postalcode') }}</th>
        <th>{{ mb_trans('text.phone') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($customers as $key => $customer)
        <tr>
          <td><a href="/customer/{{ $customer->id }}">{{ $customer->number }}</a></td>
          <td>{{ $customer->society_name??'-' }}</td>
          <td>{{ $customer->contact_name??'-' }}</td>
          <td>{{ $customer->address??'-' }}</td>
          <td>{{ $customer->city??'-' }}</td>
          <td>{{ $customer->postalcode??'-' }}</td>
          <td>{{ $customer->phone??'-' }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
