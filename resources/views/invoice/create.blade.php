@extends('layouts.app')
@section('content')
  <h1>Nouvelle facture</h1>
  <form class="" action="/invoice" method="post">
    @csrf
    <h5>Informations de facturation</h5>
    <div class="row mb-2">
      <div class="col-md-4">
        <div class="row">
          <div class="col-sm-4">{{ mb_trans('text.type') }}</div>
          <div class="col-sm-8">
            <select class="form-control" name="type">
              @foreach ($types as $key => $type)
                <option value="{{ $key }}" {{ (old('type') !== null && old('type') == $key) || (old('type') === null && $defaultType == $key) ? 'selected' :'' }}>{{ mb_trans($type) }}</option>
              @endforeach
            </select>
          </div>
        </div>
        {{-- <div class="row mt-2">
          <div class="col-sm-6">{{ mb_trans('text.invoice_number') }}</div>
          <div class="col-sm-6"><input class="form-control" type="text" name="number" value="{{ old('number', $number) }}"></div>
        </div> --}}
        <div class="row mt-2">
          <div class="col-sm-4">Type d'unité</div>
          <div class="col-sm-8">
              <select class="form-control" name="unit_type">
                  @foreach ($unitTypes as $value => $name)
                      <option value="{{$value}}" {{ old('unit_type') !== null && old('unit_type') == $value ? 'selected' :'' }}>{{$name}}</option>
                  @endforeach
              </select>
          </div>
        </div>
      </div>
      <div class="col-md-4">
          <div class="row">
            <div class="col-sm-4">{{ mb_trans('text.activities', 1) }}</div>
            <div class="col-sm-8 p-0">
              @if (count($accounts) > 1)
              <div class="input-group">
                <select class="form-control" name="account" placeholder="Activité">
                  @foreach ($accounts as $account)
                    <option value="{{ $account->id }}" {{ old('account') == $account->id ? 'selected' :'' }}>{{ $account->name }}</option>
                  @endforeach
                </select>
                {{-- <div class="input-group-append">
                  <a href="/activity/create" class="btn btn-secondary">+</a>
                </div> --}}
              </div>
            @else
              <input type="hidden" name="account" value="{{ $accounts[0]->id }}">
              <span>{{ $accounts[0]->name }}</span>
            @endif
            </div>
          </div>
        <div class="row mt-2">
          <div class="col-sm-4">{{ mb_trans('text.customer', 1) }}</div>
          <div class="col-sm-8 p-0">
            <div class="input-group">
              <select class="form-control" name="customer" placeholder="Client">
                @foreach ($customers as $customer)
                  <option value="{{ $customer->id }}" {{ old('customer') == $customer->id ? 'selected' :'' }}>{{ $customer->society_name }}</option>
                @endforeach
              </select>
              <div class="input-group-append">
                <a href="/customer/create" class="btn btn-secondary">+</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-4">{{ mb_trans('text.service') }}</div>
          <div class="col-sm-8 p-0">
            <div class="input-group">
              <select class="form-control" name="service">
                @foreach ($services as $service)
                  <option value="{{ $service->id }}" {{ old('service') == $service->id ? 'selected' :'' }}>{{ $service->name }}</option>
                @endforeach
              </select>
              <service-create class="input-group-append"></service-create>
            </div>
          </div>
        </div>
        {{-- <div class="row mt-1">
          <div class="col-sm-6">
            <div class="form-check">
              <x-form.checkbox name="isAccepted" :label="mb_trans('text.accepted')" :checked="old('isAccepted')" />
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-check">
              <x-form.checkbox name="isPaid" :label="mb_trans('text.payed')" :checked="old('isPaid')" />
            </div>
          </div>
        </div> --}}
      </div>
      <div class="col-md-4">
        <textarea class="form-control" name="note" placeholder="{{ mb_trans('text.note') }}">{{ old('note') }}</textarea>
      </div>
    </div>
    <a href="/invoice" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
    <button type="submit" class="btn btn-primary">{{ mb_trans('text.create') }}</button>
  </form>
@endsection
