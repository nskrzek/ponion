@extends('layouts.app')
@section('content')
  <form class="row mb-3" action="/invoice" method="get">
    <div class="col-md-2">
      <label for="isPaid">Type</label>
      <select class="form-control" name="type" onchange="this.form.submit()">
        @foreach ($types as $key => $type)
          <option value="{{ $key }}" {{ $filters['type'] == $key ? 'selected' :'' }}>{{ mb_trans($type) }}</option>
        @endforeach
      </select>
    </div>
    @if (in_array($filters['type'], ['', 'quote']))
      <div class="col-md-2">
        <label for="isPaid">Etat des devis</label>
        <select class="form-control" name="isAccepted" onchange="this.form.submit()">
          <option value="" {{ $filters['isAccepted'] == '' ? 'selected' :'' }}>Tous</option>
          <option value="0" {{ $filters['isAccepted'] == '0' ? 'selected' :'' }}>En attente</option>
          <option value="1" {{ $filters['isAccepted'] == '1' ? 'selected' :'' }}>Accepté</option>
        </select>
      </div>
    @endif
    @if (in_array($filters['type'], ['', 'invoice']))
      <div class="col-md-2">
        <label for="isPaid">Etat des factures</label>
        <select class="form-control" name="isPaid" onchange="this.form.submit()">
          <option value="" {{ $filters['isPaid'] == '' ? 'selected' :'' }}>Tous</option>
          <option value="0" {{ $filters['isPaid'] == '0' ? 'selected' :'' }}>En attente</option>
          <option value="1" {{ $filters['isPaid'] == '1' ? 'selected' :'' }}>Accepté</option>
        </select>
      </div>
    @endif
    <div class="col-md-2">
      <a class="btn btn-secondary btn-invoices-reset" href="/invoice" title="Réinitialiser"><i class="fas fa-undo"></i></a>
    </div>
  </form>
  <table class="table">
    <thead>
      <tr>
        <th><i class="fas fa-hashtag"></i></th>
        <th>{{ mb_trans('text.date') }}</th>
        <th>{{ mb_trans('text.type') }}</th>
        <th>{{ mb_trans('text.note') }}</th>
        <th>{{ mb_trans('text.total') }}</th>
        @if ($filters['type'] != 'invoice')
          <th>{{ mb_trans('text.accepted') }}</th>
        @endif
        @if ($filters['type'] != 'quote')
          <th>{{ mb_trans('text.payed') }}</th>
        @endif
      </tr>
    </thead>
    <tbody>
      @foreach ($invoices as $key => $invoice)
        <tr>
          <td><a href="/invoice/{{ $invoice->id }}/edit">{{ $invoice->number }}</a></td>
          @if ($invoice->type === 'quote')
              <td>{{ $invoice->date_devis?? '-' }}</td>
          @else
              <td>{{ $invoice->date_invoice?? '-' }}</td>
          @endif
          <td>{{ mb_trans('text.' . $invoice->type, 1) }}</td>
          <td>{{ $invoice->service->name?? '-' }}</td>
          <td class="text-right">{{ number_format_fr($invoice->total)?? '-' }}</td>
          @if ($filters['type'] != 'invoice')
            @if ($invoice->isAccepted)
              <td class="text-center"><i class="fas fa-check"></i></td>
            @else
              <td class="text-center"><i class="fas fa-minus"></i></td>
            @endif
          @endif
          @if ($filters['type'] != 'quote')
            @if ($invoice->isPaid)
              <td class="text-center"><i class="fas fa-check"></i></td>
            @else
              <td class="text-center"><i class="fas fa-minus"></i></td>
            @endif
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
