@extends('invoice.pdf.layout')

@section('number')
  <p>Facture # {{ $invoice->number }}</p>
@endsection

@section('other')

<p>Quantité en nombre {{$unitTypes}} ouvré.</p>
@if ($invoice->note)
  <p>Note : <br>{{ $invoice->note }}</p>
@endif
  <p>Coordonées bancaires : <br>
    IBAN: {{ $invoice->user->account->iban }}<br>
    BIC: {{ $invoice->user->account->bic }}<br>
  </p>
@endsection
