@extends('invoice.pdf.layout')

@section('number')
  <p>Devis # {{ $invoice->number }}</p>
@endsection

@section('other')

<p class="sm-infos">Quantité en nombre {{$unitTypes}} ouvré.</p>
@if ($invoice->note)
    <p>Note : <br>{{ $invoice->note }}</p>
  @endif
@endsection
