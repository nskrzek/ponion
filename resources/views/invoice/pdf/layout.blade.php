<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            color: #212529;
            font-family: sans-serif;
            font-size: 16px;
            font-weight: 400;
            line-height: 1.5;
        }

        a {
            text-decoration: none
        }

        a:hover {
            background-color: none;
            text-decoration: underline
        }

        .coord {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6rem;
        }

        .our-details>p,
        .their-details>p {
            margin: 0;
        }

        .their-details {
            float: right;
        }

        .date {
            padding-bottom: 2rem;
            margin: 0;
            text-align: right;
        }

        /* .their-details > .customer { text-align: left;} */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table.list th,
        table.list td {
            border-top: 1px solid #dee2e6;
            vertical-align: top;
            padding: .75rem;
        }

        table.list thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        table.list tr td {
            text-align: right;
        }

        table.list tr td:first-child,
        table.list th {
            font-weight: bold;
        }

        table.list .line td:first-child {
            font-weight: normal;
        }

        table.list tr td:first-child,
        table.list .line td:nth-child(2) {
            text-align: left;
        }

        table.list .spacing>td {
            padding-top: 2rem;
        }

        table.list .resum {
            text-align: right;
            column-span: 5;
        }

        .sm-infos {
            font-size: 0.75rem;
            font-style: italic;
        }

        .footer > thead > tr > th {
          text-align: left;
          font-weight: normal;
          padding-bottom: 12px;
        }
        .signature {
          border: 1px dashed #000000;
          width: 30%;
          height: 90px;
        }
    </style>
    <title>{{ ucfirst($type) }} {{ $invoice->number }}</title>
</head>

<body>
    @yield('number')

    <div class="coord">
        <div class="our-details">
            <p>{{ $invoice->user->name }}</p>
            <p>{{ $invoice->user->address }}</p>
            <p>{{ $invoice->user->cp }} {{ $invoice->user->city }}</p>
            @if ($invoice->user->website)
                <p><a href="http://{{ $invoice->user->website }}">{{ $invoice->user->website }}</a></p>
            @endif
            <p>SIRET: {{ $invoice->user->id_society }}</p>
        </div>
        <div class="their-details">
            <p>{{ $invoice->customer->society_name }}</p>
            <p>{{ $invoice->customer->contact_name }}</p>
            <p>{{ $invoice->customer->address }}</p>
            <p>{{ $invoice->customer->postalcode }} {{ $invoice->customer->city }}</p>
        </div>
    </div>
    @if ($invoice->type === 'quote')
      <div class="date">Fait le {{ $invoice->created_at->format('d/m/Y') }}</div>
    @else
      <div class="date">Fait le {{ $invoice->updated_at->format('d/m/Y') }}</div>
    @endif
    <table class="list">
        <thead>
            <tr>
                <!-- <th>Date</th> -->
                <th>Description</th>
                <th>Quantité</th>
                <th>Prix unitaire</th>
                <th>Montant (EUR)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($invoice->line as $line)
            <tr class="line">
                <!-- <td>{{ $line->date->format('d/m/Y') }}</td> -->
                <td>{{ ucfirst($line->description) }}</td>
                <td>{{ $line->quatity }}</td>
                <td>{{ $line->unit_price }}</td>
                <td>{{ $line->sub_total }}</td>
            </tr>
            @endforeach
            <tr>
                <td>Sous-total</td>
                <td colspan="3" class="resum">{{ $invoice->total_brut ? $invoice->total_brut :'0.00' }}</td>
            </tr>
            <tr>
                <td>TVA</td>
                <td colspan="3" class="resum">{{ $invoice->taxe ? $invoice->taxe : '0.00' }}</td>
            </tr>
            <tr>
                <td>Total</td>
                <td colspan="3" class="resum">{{ $invoice->total ? $invoice->total :'0.00' }}</td>
            </tr>
        </tbody>
    </table>
    <div class="other">
      @yield('other')
    </div>
    @if ($invoice->type === 'quote')
      {{-- For obscure reason flex style don't working --}}
      <table class="footer">
        <thead>
          <tr>
            <th></th>
            <th>Signature</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
            <td class="signature"></td>
          </tr>
        </tbody>
      </table>
    @endif
</body>

</html>
