@extends('layouts.app')
@section('content')
  <form action="/invoice/{{ $invoice->id }}" method="post">
    @csrf
    @method('PUT')
    <h5>Informations de facturation</h5>
    <div class="row mb-4">
      <div class="col-md-4">
        <div class="row">
          <div class="col-sm-6">{{ mb_trans('text.type') }}</div>
          <div class="col-sm-6">
            <select class="form-control" name="type">
              @foreach ($types as $key => $type)
                <option value="{{ $key }}" {{ old('type', $invoice->type) == $key ? 'selected' :'' }}>{{ mb_trans($type) }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-6">Type d'unité</div>
          <div class="col-sm-6">
            @if ($invoice->isAccepted)
              <p>{{ $unitTypes[$invoice->unit_type] }}</p>
              <input type="hidden" name="unit_type" value="{{ $invoice->unit_type }}">
            @else
              <select class="form-control" name="unit_type">
                @foreach ($unitTypes as $value => $name)
                  <option value="{{$value}}" {{ old('unit_type', $invoice->unit_type) === $value ? 'selected' :'' }}>{{$name}}</option>
                @endforeach
              </select>
            @endif
          </div>
        </div>
        <div class="row mt-2">
            @if ($invoice->type === 'quote')
                <div class="col-sm-6">{{ mb_trans('text.devis_number') }}</div>
            @else
                <div class="col-sm-6">{{ mb_trans('text.invoice_number') }}</div>
            @endif
          <div class="col-sm-6">
            <p>{{ $invoice->number }}</p>
          </div>
        </div>
        <div class="row">
            @if ($invoice->type === 'quote')
                <div class="col-sm-6">{{ mb_trans('text.date_devis') }}</div>
                <div class="col-sm-6">{{$invoice->created_at->format('d/m/Y')}}</div>
            @else
                <div class="col-sm-6">{{ mb_trans('text.date_invoice') }}</div>
                <div class="col-sm-6">{{$invoice->updated_at->format('d/m/Y')}}</div>
            @endif
        </div>
        <div class="row pt-2">
          <div class="col-sm-6">Total HT</div>
          <div class="col-sm-6">{{ number_format_fr($invoice->total_brut) }} €</div>
        </div>
        <div class="row">
          <div class="col-sm-6">TVA</div>
          <div class="col-sm-6">{{ $invoice->taxe??'0,00' }} %</div>
        </div>
        <div class="row">
          <div class="col-sm-6">Total TTC</div>
          <div class="col-sm-6">{{ number_format_fr($invoice->total) }} €</div>
        </div>
      </div>
      <div class="col-md-4">
          <div class="row">
            <div class="col-sm-4">{{ mb_trans('text.activities', 1) }}</div>
            <div class="col-sm-8 p-0">
              @if (count($accounts) > 1)
              <div class="input-group">
                <select class="form-control" name="account" placeholder="Activité">
                  @foreach ($accounts as $account)
                    <option value="{{ $account->id }}" {{ old('account', $invoice->account_id) == $account->id ? 'selected' :'' }}>{{ $account->name }}</option>
                  @endforeach
                </select>
                {{-- <div class="input-group-append">
                  <a href="/activity/create" class="btn btn-secondary">+</a>
                </div> --}}
              </div>
            @else
              <input type="hidden" name="account" value="{{ old('account', $invoice->account_id) }}">
              <span>{{ $accounts[0]->name }}</span>
            @endif
            </div>
          </div>
        <div class="row mt-2">
          <div class="col-sm-4">{{ mb_trans('text.customer', 1) }}</div>
          <div class="col-sm-8 p-0">
            <div class="input-group">
              @if ($invoice->isAccepted)
                <p>{{ $invoice->customer->society_name }}</p>
                <input type="hidden" name="customer" value="{{ $invoice->customer->id }}">
              @else
                <select class="form-control" name="customer" placeholder="Client">
                  @foreach ($customers as $key => $customer)
                    <option value="{{ $customer->id }}" {{ old('customer', $invoice->customer_id) == $customer->id ? 'selected' :'' }}>{{ $customer->society_name }}</option>
                  @endforeach
                </select>
                <div class="input-group-append">
                  <a href="/customer/create" class="btn btn-secondary">+</a>
                </div>
              @endif
            </div>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-4">{{ mb_trans('text.service') }}</div>
          <div class="col-sm-8 p-0">
            <div class="input-group">
              @if ($invoice->isAccepted)
                <p>{{ $invoice->service->name }}</p>
                <input type="hidden" name="service" value="{{ $invoice->service->id }}">
              @else
                <select class="form-control" name="service" {{ $invoice->isPaid ? 'disabled' : '' }}>
                  @foreach ($services as $key => $service)
                    <option value="{{ $service->id }}" {{ old('service', $invoice->service_id) == $service->id ? 'selected' :'' }}>{{ $service->name }}</option>
                  @endforeach
                </select>
                <service-create class="input-group-append"></service-create>
              @endif
            </div>
          </div>
        </div>
        <div class="row mt-1">
          @if ($invoice->type == 'quote')
            <div class="col-sm-6">
              <div class="form-check">
                <x-form.checkbox name="isAccepted" :label="mb_trans('text.accepted')" :checked="old('isAccepted', $invoice->isAccepted)" />
              </div>
            </div>
          @else
            <div class="col-sm-6">
              <div class="form-check">
                <x-form.checkbox name="isPaid" :label="mb_trans('text.payed')" :checked="old('isPaid', $invoice->isPaid)" />
              </div>
            </div>
          @endif
        </div>
      </div>
      <div class="col-md-4">
        <textarea class="form-control h-100" name="note" placeholder="{{ mb_trans('text.note') }}">{{ old('note', $invoice->note) }}</textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
          <a href="/invoice" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
          <button type="submit" class="btn btn-primary">{{ mb_trans('text.update') }}</button>
      </div>
      <div class="col-md-4">
      @if (!$invoice->isPaid && !$invoice->isAccepted)
          <new-invoice-line :invoideid="{{ $invoice->id }}"></new-invoice-line>
      @endif
    </div>
      <div class="col-md-4">
          <a href="/generate-pdf/{{ $invoice->id }}" class="btn btn-secondary" target="_blanc">Imprimer</a>
          @if (!$invoice->isAccepted)
              <delete-button></delete-button>
          @endif
      </div>
    </div>
  </form>
  @if (!$invoice->isPaid)
      <form action="/invoice/{{ $invoice->id }}" method="post" name="del_form">
          @csrf
          @method('DELETE')
      </form>
  @endif
  <app-invoice-line-list invoideid="{{ $invoice->id }}" is-accepted="{{ $invoice->isAccepted }}" is-paid="{{ $invoice->isPaid }}"></app-invoice-line-list>
@endsection
