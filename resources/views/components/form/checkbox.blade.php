<div class="custom-control custom-checkbox" type="checkbox">
  <input type="checkbox" name="{{ $name }}" id="{{ $name }}" class="custom-control-input" autocomplete="off" value="1"
  {{ $checked ? 'checked' :'' }}
  >
  <label class="custom-control-label" for="{{ $name }}">
    {{ $label}}
  </label>
</div>
