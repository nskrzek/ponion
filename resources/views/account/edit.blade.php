@extends('layouts.app')
@section('content')
  <form action="/account/{{$account->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group mb-3">
      <input type="text" class="form-control" name="name" value="{{ old('name', $account->name) }}" placeholder="{{ mb_trans('text.name') }}">
    </div>
    <div class="form-group row mb-3">
      <div class="input-group col-md-6">
        <input type="text" class="form-control" name="iban" value="{{ old('iban', $account->iban) }}" placeholder="IBAN">
      </div>
      <div class="input-group col-md-6">
        <input type="text" class="form-control" name="bic" value="{{ old('bic', $account->bic) }}" placeholder="BIC">
      </div>
    </div>
    <div class="form-group row mb-3">
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="taxe" step="0.01" max="100.00" min="0.00" value="{{ old('taxe', $account->taxe) }}" placeholder="{{ mb_trans('text.taxation') }}">
        <div class="input-group-append">
          <span class="input-group-text">%</span>
        </div>
      </div>
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="business_digit" value="{{ old('business_digit', $account->business_digit) }}" placeholder="{{ mb_trans('text.business_digit') }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div>
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="profits" value="{{ old('profits', $account->profits) }}" placeholder="{{ mb_trans('text.profits', 1) }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div>
      {{-- <div class="input-group col-md-3">
        <input type="number" class="form-control" name="spending" value="{{ old('spending', $account->spending) }}" placeholder="{{ mb_trans('text.spending', 1) }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div> --}}
    </div>
    <a href="/activity/{{$account->id}}" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
    <button type="submit" class="btn btn-primary">{{ mb_trans('text.update') }}</button>
  </form>
@endsection
