@extends('layouts.app')
@section('content')
  <div id="profile">
    <div class="card">
      <div class="card-header">
        <h3>{{ $account->name }}</h3>
      </div>
      <div class="card-body">
        <ul>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.iban') }} :</span><span class="col-sm-6">{{ $account->iban??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.bic') }} :</span><span class="col-sm-6">{{ $account->bic??'-' }}</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.taxation') }} :</span><span class="col-sm-6">{{ number_format_fr($account->taxe)??'0,0' }} %</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.business_digit') }} :</span><span class="col-sm-6">{{ number_format_fr($account->business_digit)??'0,0' }} €</span></li>
          <li class="row"><span class="col-sm-6">{{ mb_trans('text.profits', 2) }} :</span><span class="col-sm-6">{{ number_format_fr($account->profits)??'0,0' }} €</span></li>
          <!-- <li class="row"><span class="col-sm-6">{{ mb_trans('text.spending', 2) }} :</span><span class="col-sm-6">{{ $account->spending??'-' }}</span></li> -->
        </ul>
      </div>
      <div class="card-footer">
        <a href="/activity" class="btn btn-primary">{{ mb_trans('text.back') }}</a>
        <a class="btn btn-secondary" href="/activity/{{ $account->id }}/edit">{{ mb_trans('text.edit') }}</a>
      </div>
    </div>
  </div>
@endsection
