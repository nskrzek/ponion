@extends('layouts.app')
@section('content')
  <table class="table">
    <thead>
      <tr>
        <th>{{ mb_trans('text.name') }}</th>
        <th class="number">{{ mb_trans('text.business_digit', 2) }}</th>
        <th class="number">{{ mb_trans('text.profits', 2) }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($accounts as $key => $account)
        <tr>
          <td><a href="/activity/{{ $account->id }}">{{ $account->name }}</a></td>
          <td class="number">{{ number_format_fr($account->business_digit) . ' €'?? '-' }}</td>
          <td class="number">{{ number_format_fr($account->profits) . ' €'?? '-' }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
