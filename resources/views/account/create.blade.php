@extends('layouts.app')
@section('content')
  <h1>{{ mb_trans('text.new') }} {{ mb_trans('text.account', 1, [], false) }}</h1>
  <form action="/account" method="post" class="mt-5">
    @csrf
    <div class="form-group mb-3">
      <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="{{ mb_trans('text.name') }}">
    </div>
    <div class="form-group row mb-3">
      <div class="input-group col-md-6">
        <input type="text" class="form-control" name="iban" value="{{ old('iban') }}" placeholder="IBAN">
      </div>
      <div class="input-group col-md-6">
        <input type="text" class="form-control" name="bic" value="{{ old('bic') }}" placeholder="BIC">
      </div>
    </div>
    <div class="form-group row mb-3">
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="taxe" step="0.01" max="1" min="0.00" value="{{ old('taxe') }}" placeholder="{{ mb_trans('text.taxation') }}">
        <div class="input-group-append">
          <span class="input-group-text">%</span>
        </div>
      </div>
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="business_digit" value="{{ old('business_digit') }}" placeholder="{{ mb_trans('text.business_digit') }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div>
      <div class="input-group col-md-4">
        <input type="number" class="form-control" name="profits" value="{{ old('profits') }}" placeholder="{{ mb_trans('text.profits', 1) }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div>
      {{-- <div class="input-group col-md-4">
        <input type="number" class="form-control" name="spending" value="{{ old('spending') }}" placeholder="{{ mb_trans('text.spending', 1) }}">
        <div class="input-group-append">
          <span class="input-group-text">€</span>
        </div>
      </div> --}}
    </div>
    <a href="/activity" class="btn btn-secondary">{{ mb_trans('text.back') }}</a>
    <button type="submit" class="btn btn-primary">{{ mb_trans('text.create') }}</button>
  </form>
@endsection
