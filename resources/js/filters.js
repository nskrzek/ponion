import Vue from 'vue/dist/vue'
import {yesNo, ucfirst, formatThousandth, formatPhoneNumber} from './supports'

Vue.filter('yesNo', val => yesNo(val));
Vue.filter('ucfirst', str => ucfirst(str));
Vue.filter('formatThousandth', number => formatThousandth(number));
Vue.filter('formatPhoneNumber', number => formatPhoneNumber(number));
