let syncedSecondary = true;
/**
 * Add an event on a element
 *
 * @param {Object} el           The element
 * @param {String} eventName    Event name (ex: click, change...)
 * @param {Function} eventHandler Callback function
 */
window.addEvent = (el, eventName, eventHandler) => {
  if (el.addEventListener) {
    el.addEventListener(eventName, eventHandler, false);
  } else if (el.attachEvent) {
    el.attachEvent("on" + eventName, eventHandler);
  }
}

/**
 * Check if the value passed in parameter is a function
 *
 * @param  {Function} fn [description]
 * @return {Boolean}     [description]
 */
window.isFunction = (fn) => {
  return fn !== null && typeof fn === 'function';
}

/**
 * Add an event on a element. but check if the dom and callback function exist
 *
 * @param  {Object}   dom   The element
 * @param  {String}   event Event name (ex: click, change...)
 * @param  {Function} fn    Callback function
 * @return {Null}           [description]
 */
window.onEvent = (dom, event, fn) => {
  // Do nothing if don is undefined
  if (null == dom)
    return;

  if (!isFunction(fn)) {
    console.error("Callback function does not exist");
    return;
  }

  addEvent(dom, event, fn);
}

/**
 * Make a smooth scroll
 *
 * @param  {Object<element>}                          target      element where scroll stop
 * @param  {Object<speed, acceleration, speedLimit>}  params      Parameters
 */
window.smoothScroll = (target, params = {}) => {
  if (target == null) {
    return;
  }
  let currentScroll = window.pageYOffset,
   targetPos = target.getBoundingClientRect().top + currentScroll,
   distanceToTarget = targetPos - currentScroll,
   toTop = false,
   speed = params.speed || 1,
   acceleration = params.acceleration || 1.085,
   speedLimit = params.speedLimit || 100;

   if (distanceToTarget < 0) {
		toTop = true;
		speed *= -1;
	}

  const scroll = () => {

    let currentScroll = window.pageYOffset,
		 targetPos = target.getBoundingClientRect().top + currentScroll,
		 distanceToTarget = targetPos - currentScroll;

    if (distanceToTarget < speed && !toTop) {
			scrollBy(0, distanceToTarget);
		} else {
			scrollBy(0, speed);
			let speedToLimitSpeed = speedLimit - speed,
			 acceleratedSpeed = speed * acceleration;

			if (speedToLimitSpeed - speed > acceleratedSpeed ) {
         speed *= acceleration;
      } else {
        speed = speedLimit;
      }

			if (toTop && distanceToTarget == 0) return;
			requestAnimationFrame(scroll);
		}
  };

  scroll();
}
window.removeTextNode = (nodes) => {
    return [].filter.call(nodes, function(o){
        return o.nodeType == Node.ELEMENT_NODE;
    });
}

window.forEach = (array, callback, scope) => {
  let i = 0, arrLength = array.length;
  for (; i < arrLength; i++) {
    // the first argument is thisArg which is the context and can used as `this` in the callback
    callback.call(scope, array[i], i); // passes back stuff we need
  }
}

window.toggleChecked = (elem) => {
  if (elem.checked) {
    elem.checked = false;
  } else {
    elem.checked = true
  }
}

window.addClass = (el, className) => {
    if (el.classList) {
        el.classList.add(className);
    } else {
        el.className += ' ' + className;
    }
}

window.removeClass = (el, className) => {
    el.classList.remove(className);
}

window.toggleClass = (el, classe) => {
  if (el.classList) {
    el.classList.toggle(classe);
  }
}
