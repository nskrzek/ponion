addEvent(document, 'DOMContentLoaded', () => {
  /**
   * Display an element
   *
   * @var data-popin  element targeted
   */
  if (document.querySelectorAll('*[data-popin]').length > 0) {
    [].forEach.call(document.querySelectorAll('*[data-popin]'), (el)=> {
      onEvent(el, 'click', (e) => {
        if (document.getElementById(e.target.dataset.popin) != 'undefined') {
          document.getElementById(e.target.dataset.popin).style.display = 'block';
        }
      });
    });
  }

  /**
   * Scroll (smoothly) to an element
   *
   * @var data-scroll  element targeted
   */
  if (document.querySelectorAll('*[data-scroll]').length > 0) {
    [].forEach.call(document.querySelectorAll('*[data-scroll]'), (el)=> {
      onEvent(el, 'click', (e) => {
        if (e.target.dataset.scroll !== 'undefined' && document.getElementById(e.target.dataset.scroll) != null) {
          smoothScroll(document.getElementById(e.target.dataset.scroll), {speed: 10});
        } else if (e.target.parentNode.dataset.scroll !== 'undefined' && document.getElementById(e.target.parentNode.dataset.scroll) != null) {
          smoothScroll(document.getElementById(e.target.parentNode.dataset.scroll), {speed: 10});
        }
        return;
      });
    });
  }

/**
 * Toggle checkbox not in vuejs
 */
  if (document.querySelectorAll('.checkbox').length > 0) {
    [].forEach.call(document.querySelectorAll('.checkbox'), function (el) {
      let rows = el.childNodes;
      rows = removeTextNode(rows);
      forEach(rows, (row) => {
        if (row.getAttribute('for') != null) {
          onEvent(row, 'click', (e) => {
            let inputName = e.target.getAttribute('for');
            let input = document.getElementsByName(inputName);
            toggleChecked(input[0]);
          });
        }
      });
    });
  }
  if (document.getElementById('logout-link') != null && document.getElementById('logout-form') != null) {
    onEvent(document.getElementById('logout-link'), 'click', (e) => {
      document.getElementById('logout-form').submit();
    });
  }
  /*if (document.getElementById('mobile-menu') != null && document.getElementById('mobile-menu-icon') != null) {
    onEvent(document.getElementById('mobile-menu-icon'), 'click', (e) => {

      // toggleClass(document.body, 'menu-opend');
      toggleClass(document.getElementById('mobile-menu-icon'), 'active');
      toggleClass(document.getElementById('mobile-menu'), 'active');
    });
  }*/
});
