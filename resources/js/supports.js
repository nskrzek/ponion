export default {
  install(Vue, options) {
    Vue.prototype.$handlingErrors = (error) => handlingErrors(error)
    Vue.prototype.$roundFloat = (n, x) => roundFloat(n, x)
  }
}
export const yesNo = (v) => {
  return (v === 1?'Oui':'Non');
}

export const ucfirst = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export const formatThousandth = (number, separator = ' ') => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
}

export const formatPhoneNumber = (number, separator = ' ') => {
    return number.toString().replace(/\B(?=(\d{2})+(?!\d))/g, separator);
}

/**
 * Handling axios errors
 * @param  {Object} error
 */
export const handlingErrors = (error) => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.error('response:');
        console.error(error.response.data);
        console.error(error.response.status);
        // console.log(error.response.headers);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.error('request:');
        console.error(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.error('Error', error.message);
    }
    // console.log('config:');
    // console.log(error.config);
}

/**
 * Round a float
 *
 * @param  {Float}  n Value must be rounded
 * @param  {Int}    x Number of digit after comma
 * @return {Float}    Result rounded
 */
export const roundFloat = (n, x) => {
    n = parseFloat(n);
    return Number(n.toFixed(x));
}
