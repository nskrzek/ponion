
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./components/scripts');
require('./components/events');
import Vue from 'vue/dist/vue'

if (window.Vue) {
  window.Vue = Vue;
}

import Support from './supports'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import VeeValidate from 'vee-validate'
import InvoiceLineList from '@/components/invoice/line/list'
import NewInvoiceLine from '@/components/invoice/line/new'
import ServiceCreate from '@/components/services/create'
import DeleteButton from '@/components/DeleteButton'
import DropdownProfile from '@/components/DropdownProfile'

Vue.use(Support);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(require('vue-moment'));
Vue.use(VeeValidate, {
  // This is the default
  inject: true,
  // Important to name this something other than 'fields'
  fieldsBagName: 'veeFields'
})

Vue.filter('yesNo', val => Support.yesNo(val));
Vue.filter('ucfirst', str => Support.ucfirst(str));
Vue.filter('formatThousandth', number => Support.formatThousandth(number));
Vue.filter('formatPhoneNumber', number => Support.formatPhoneNumber(number));

Vue.component('app-invoice-line-list', InvoiceLineList);
Vue.component('service-create', ServiceCreate);
Vue.component('new-invoice-line', NewInvoiceLine);
Vue.component('delete-button', DeleteButton);
Vue.component('dropdown-profile', DropdownProfile);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
