<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('customer_id');
            $table->integer('service_id');
            $table->string('date');
            $table->string('type')->nullable();
            $table->string('number');
            $table->boolean('isAccepted')->default(false);
            $table->boolean('isPaid')->default(false);
            $table->string('ref')->nullable();
            $table->double('total', 9, 2)->nullable();
            $table->text('note')->nullable();
            $table->uuid('uuid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
