<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountOnInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasColumn('invoices', 'account_id')) {
        Schema::table('invoices', function (Blueprint $table) {
          $table->integer('account_id')->nullable()->after('service_id');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasColumn('invoices', 'account_id')) {
        Schema::table('invoices', function (Blueprint $table) {
          $table->dropColumn('account_id');
        });
      }
    }
}
