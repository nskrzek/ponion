<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_lines', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->integer('invoice_id');
          $table->string('date');
          $table->boolean('isCharged')->default(false);
          $table->text('description')->nullable();
          $table->tinyInteger('quatity')->nullable();
          $table->double('unit_price', 9, 2)->nullable();
          $table->string('discount_type', 2)->default('%');
          $table->string('discount_mode', 2)->default('<');
          $table->double('discount', 9, 2)->nullable();
          $table->boolean('isTaxable')->default(false);
          $table->boolean('taxes_included')->default(false);
          $table->double('sub_total', 9, 2)->nullable();
          $table->double('taxe', 9, 2)->nullable();
          $table->uuid('uuid');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_lines');
    }
}
