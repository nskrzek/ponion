<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxesAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasColumn('accounts', 'taxe')) {
        Schema::table('accounts', function (Blueprint $table) {
          $table->float('taxe', 3, 2)->default(0.0)->after('bic');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasColumn('accounts', 'taxe')) {
        Schema::table('accounts', function (Blueprint $table) {
          $table->dropColumn('taxe');
        });
      }
    }
}
