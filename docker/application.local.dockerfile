#########################################################################
####################### SYSTEM ENVIRONMENT BUILD ########################
#########################################################################

# Inspired by lorisleivaleiva conf: https://github.com/lorisleiva/laravel-docker/

# MarisDB: https://github.com/Ara4Sh/docker-alpine-mariadb

FROM php:7.4-alpine
LABEL maintainer="Nicolas Skrzek"

# Install dev dependencies
RUN apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    oniguruma-dev \
    libsodium-dev \
    libxml2-dev \
    freetype-dev \
    libjpeg-turbo-dev

# Install system dependencies
RUN apk add --no-cache \
    bash \
    curl \
    g++ \
    gcc \
    git \
    libc-dev \
    libpng-dev \
    oniguruma \
    libsodium \
    mariadb \
    mariadb-client \
    nodejs \
    nodejs-npm \
    openssl \
    jpegoptim \
    optipng \
    pngquant \
    php7-fpm \
    gifsicle \
    zlib-dev \
    libzip-dev

## Install and enable php extensions

# PHP pdo_mysql extension
RUN docker-php-ext-install pdo_mysql

# PHP php-int extension
# RUN docker-php-ext-install intl

# PHP tokenizer extension
RUN docker-php-ext-install tokenizer

# PHP mbstring extension
RUN docker-php-ext-install mbstring

# PHP sodium extension
RUN docker-php-ext-install sodium

# PHP add
RUN docker-php-ext-install opcache

# PHP zip extension
# RUN docker-php-ext-install zip

# PHP bcmath extension
# RUN docker-php-ext-install bcmath

# PHP memcached extension
# RUN pecl install memcached && docker-php-ext-enable memcached

# PHP GD Library extension
RUN docker-php-ext-install gd

# PHP exif extension
# RUN docker-php-ext-install exif

# Configure PHP-FPM
COPY php-fpm.d/ /etc/php7/php-fpm.d/

# COMPOSER and add its bin to the PATH
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"

# Create user gitlab
# RUN addgroup gitlab --gid 1024
# RUN adduser  gitlab --gecos "" --disabled-password --ingroup gitlab
# RUN echo "gitlab:asecretpassword" | sudo chpasswd

# Allow container to write on host
# RUN usermod -u 1000 gitlab

# Sets the user name and optionally group (user:group) to use when running the image and RUN command
# USER gitlab:gitlab

# Workdir
WORKDIR /app

# Copy opcache config
COPY docker/php/conf.d/docker-php-ext-opcache.ini /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

COPY nginx/laravel.conf /etc/nginx/conf.d/default.conf
COPY nginx/rewrites /etc/nginx/rewrites

#########################################################################
########################### APPLICATION BUILD ###########################
#########################################################################

#RUN npm install -g svgo

# Node dependencies (copy only package.json for cache purpose)
#COPY package.json .
#RUN npm install

# Composer dependecies (copy only composer.json for cache purpose)
#COPY composer.json .
#COPY database/ database/
#RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

# This is where all the source code is imported in the container
# When image is built in Gitlab-CI, a clone of the repository is made automatically
# and all the source code is available at the root directory
#COPY . .

#RUN cp .env.staging .env
#RUN php artisan key:generate

# FIX PERMISSIONS - Desactivated for now... Need to check if we need it or not
# RUN find /app -type f -exec chmod 644 {} \;
# RUN find /app -type d -exec chmod 755 {} \;
#RUN chmod -R 777 /app/storage
#RUN chmod -R 777 /app/bootstrap/cache/
# RUN find /app/node_modules -type f -name '*.js' -exec chmod 775 {} \;

#RUN npm run production
#RUN npm run watch

## Certbot
# RUN apk add --no-cache certbot certbot-nginx

# VOLUME /etc/letsencrypt
# VOLUME /var/lib/letsencrypt

# ENTRYPOINT []
# CMD /usr/bin/certbot certonly --webroot -w /var/www/certbot --email nicolas@fstck.co -d spi.fstck.co --rsa-key-size 4096 --agree-tos --force-renewal
