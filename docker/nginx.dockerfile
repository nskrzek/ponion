
FROM nginx:alpine
LABEL maintainer="Nicolas Skrzek"

RUN apk add --update nodejs nodejs-npm

COPY nginx/laravel.conf /etc/nginx/conf.d/default.conf
COPY nginx/rewrites /etc/nginx/rewrites

# Workdir
WORKDIR /app

#########################################################################
########################### APPLICATION BUILD ###########################
#########################################################################

RUN npm install -g svgo

# Node dependencies (copy only package.json for cache purpose)
COPY package.json .

RUN npm install

COPY . .

RUN npm run production
