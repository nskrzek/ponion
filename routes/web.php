<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
  AccountController,
  CustomerController,
  InvoiceLineController,
  InvoiceController,
  ServiceController,
  ProfilController,
  HomeController
};

Route::get('/', function () {
  return redirect('/invoice/');
});

Route::resources([
  'activity' => AccountController::class,
  'customer' => CustomerController::class,
  'invoice/line' => InvoiceLineController::class,
  'invoice' => InvoiceController::class,
  'service' => ServiceController::class,
  'profil' => ProfilController::class,
]);

Route::post('invoice/line/create/{invoiceId}', [InvoiceLineController::class, 'store']);
Route::get('generate-pdf/{invoiceId}', [InvoiceController::class, 'generatePDF']);

// Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
